package main

import (
	"fmt"
	"math/big"
	"os"

	log "github.com/Sirupsen/logrus"
)

// convenience function
func mul(x, y *big.Int) *big.Int {
	return big.NewInt(0).Mul(x, y)
}

// play with big integers.
func factorial(n int) *big.Int {
	// Calculate 18!  that is, calculate 18 factorial:

	fac1 := big.NewInt(1)

	// 18! fits in int64, but not 19!
	for i := 2; i <= n; i++ {
		var i2 = int64(i)
		fac2 := big.NewInt(i2)
		//fac1 = fac1 * fac2
		fac1 = mul(fac1, fac2)

		/* // if we were using regular integers, we'd get silent overflows.
		if fac1 <= 0 {
			fmt.Println("overflow! ", i)
			break
		}
		*/

	}
	// 18! = 6402373705728000
	// 19! = 121645100408832000 (does not fit in 64 bit integer)
	// (sixteen significant figures of precision)
	return fac1
}

func main() {

	// redirect logrus to file
	f, err := os.OpenFile(`application.log`, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("Error opening file: %v", err)
	}
	log.SetOutput(f)
	log.Info("My little test application started.")

	fac1 := factorial(19)
	fmt.Println("Hello World", fac1)

}
